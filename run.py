from flask import Flask, render_template, request, jsonify, make_response
from geopy.geocoders import Nominatim
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

geolocator = Nominatim(user_agent= "app")

#Database connection
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://postgres:password@localhost/postgres'
db = SQLAlchemy(app)

#Creating a table object 
report = db.Table('amazing_report_3', db.metadata, autoload_with=db.engine)

@app.errorhandler(404)
def not_found(e):
    return render_template("404.html") 

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/coordinatesForm')
def coordinates():
    return render_template('coordinatesForm.html')

@app.route('/address-form')
def addressForm():
    return render_template('address-form.html')

@app.route('/thankyou')
def thankyou():
    return render_template('thankyou.html')


# Part 1 - endpoint which takes geo-coordinates as input, and returns the physical address
@app.route('/process', methods=["POST"])
def process():
    if request.method == "POST":

        # Receiving latitude and longitude values from the form
        latitude = request.form.get("latitude") 
        longitude = request.form.get("longitude")
        coordinates = latitude + ', ' + longitude

        location = geolocator.reverse(coordinates, exactly_one=True)
        address = location.raw['address']

        result = make_response(jsonify(address), 200)  
        return result


# part 1 - endpoint which takes an address, and returns geo-coordinates (lat,lng) 
@app.route('/address', methods=["GET","POST"])
def address():
    if request.method == "POST":
        #Values from UI
        streetNumber = request.form.get("streetNumber")
        streetName = request.form.get("streetName")
        province = request.form.get("province")
        addrs = streetNumber + ' ' + streetName + ', ' + province

        location = geolocator.geocode(addrs)
        latitude = location.latitude
        longitude = location.longitude
        print(latitude,longitude)
    return render_template('thankyou.html', latitude=latitude, longitude=longitude)


#Part 2 - endpoint
@app.route('/report', methods=['GET', 'POST'])
def amazing_rep():

    if request.method == 'GET':
        #Extracting all data in amazing_report_3 table
        results = db.session.query(report).all()

        #Converting each column data into JSON
        res = [
            {
                'id': r.id,
                'uuid': r.uuid,
                'user_id': r.user_id,
                'place_id': r.place_id,
                'latitude': r.latitude,
                'longitude': r.longitude,
                'successful':r.successful,
                'method':r.method
            } for r in results]
        jsonResult = make_response(jsonify(res), 200) 
        print(type(jsonResult))
        return jsonResult

    else:
        return make_response(jsonify({"message": "Access Denied"}), 400)


if __name__ == '__main__':
    app.run(host='0.0.0.0', use_reloader=True)
